# Instruçoes que precisei usar para executar react native

# terminal/prompt e execute o comando adb devices

```
adb reverse tcp:8081 tcp:8081
adb forward tcp:8083 tcp:8083
adb device -l
```

# trocar versao java
```
sudo update-alternatives --config java
```

# limpar
```
watchman watch-del-all
watchman shutdown-server
watchman shutdown-server; WATCHMAN_CONFIG_FILE=~/watchman watchman --foreground --logfile=/dev/stdout --no-save-state --statefile=/dev/null
```
# expo.io
```
yarn add react-native@https://github.com/expo/react-native/archive/sdk-36.0.0.tar.gz
yarn add expo-cli
yarn add expo
expo start
expo r -c
```
## Update your Expo SDK Version

    Open the app.json file from the project and change sdkVersion to 36.0.0

    Update your Dependancies

    Open the package.json file and update the following dependencies,
        Update the jest-expo to ^33.0.0 in devDependencies
        Update the react-native to ^0.59.10 in dependencies
        Update the expo to ^33.0.0 in dependencies
        Update the react to ^16.8.3 in dependencies
```
rm -rf node_modules/* && npm i && expo start -c
```

## link unlink
```
react-native unlink react-native-gesture-handler

yarn add jetifier -D ou npm install --save-dev jetifier

yarn add @react-native-community/async-storage
react-native link @react-native-community/async-storage
react-native unlink @react-native-community/async-storage
```


## react-native.config.js para ficar assim:
```js
// react-native.config.js
module.exports = {
  dependencies: {
    'some-unsupported-package': {
      platforms: {
        android: null, // disabilitar na plataforma Android, outras plataformas ainda utilizaram o autolink se fornecido.
      },
    },
  },
};
```
## watchmanconfig
```sh
touch .watchmanconfig
watchman watch-del-all
watchman shutdown-server
sudo pkill -9 -x fseventsd
echo fs.inotify.max_user_instances=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
echo fs.inotify.max_queued_events=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
```

## edite ~/.profile
```sh
~/.profile

$ export ANDROID_HOME=~/Android/Sdk
$ export PATH=$PATH:$ANDROID_HOME/tools
$ export PATH=$PATH:$ANDROID_HOME/platform-tools

export ANDROID_HOME=/usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
~/Android/Sdk/tools/bin/sdkmanager "platform-tools" "platforms;android-27" "build-tools;27.0.3"
```

# comando para executar react
```sh
react-native run-android
react-native run-android --verbose
react-native start
```

## links
https://docs.rocketseat.dev/ambiente-react-native/usb/android

- Renato Lucena - 15/03/2020
